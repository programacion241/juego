#include <stdio.h>
#include <windows.h>
#define V 26
#define H  88

void juego(char tablero[V][H],int bolx,int boly,int jugin,int jugfin,int opin,int opfin);
void limite(char tablero [V][H]);
void curjug(char tablero[V][H],int jugin,int jugfin);
void curop(char tablero[V][H],int opin,int opfin);
void curbol(char tablero[V][H],int bolx, int boly);
//void verejemplo(char tablero[V][H]);
void nuevojuego(char tablero[V][H],int bolx,int boly,int jugin,int jugfin,int opin,int opfin,int movx,int movy,int movop);
void dibujar(char tablero[V][H]);
void modificar(char tablero[V][H],int *bolx,int *boly,int *jugin,int *jugfin,int *opin,int *opfin,int *movx,int *movy,int *movop,int *gol); 
void actualizar(char tablero[V][H],int bolx,int boly,int jugin,int jugfin,int opin,int opfin);  



int main (){
   //VARIABLES PARA EL DISE�O DEL TABLERO, CURSORES Y DEMAS
    
	int inicio;
	int bolx,boly,jugin,jugfin,opin,opfin;
	char tablero[V][H];
	
	bolx=37;	boly=10;
	
	jugin=8;   jugfin = 12;
	
	opin=4;    opfin=20;
	
	//VARIABLES DE CAMBIO DE POSICION 
	int movx,movy,movop;
	
     movx=-1;
	 movy=-1; 
	movop=-1;
	
	printf("\t\t\tutiliza las teclas 8 y 2 para mover tu cursor\n\n");
	printf("\t\t\tpresiona un numero y enter para empezar\n");
	scanf("%d",&inicio);
	
	juego(tablero,bolx,boly,jugin,jugfin,opin,opfin);
	
    nuevojuego(tablero,bolx,boly,jugin,jugfin,opin,opfin,movx,movy,movop);


     
    system("pause");
	return 0;
	
}
 
 
 void juego(char tablero[V][H],int bolx,int boly,int jugin,int jugfin,int opin,int opfin){
 	
 	 limite(tablero);
 	
	  curjug(tablero,jugin,jugfin);
 	
 	   curop(tablero,opin,opfin);
 	
 	    curbol(tablero,bolx,boly);
 	
 }
 
 void limite(char tablero [V][H]){
 	
	 int i,j;
 	
 	for (i=0;i< V;i++){
	  
	  for (j=0;j<H;j++){
	  	
		  if(i==0 || i== V-1){
	  	
		  	tablero [i][j]= '*';
		  }
	 	
		 else if(j==0||j==H-1){
	 		
			 tablero [i][j]= '*';
		 }
		 else{
		 	tablero [i][j]=' ';
		 }
	 }
	
	 }
	 
 		
 }
 
 void curjug(char tablero[V][H],int jugin,int jugfin){
 
 	int i,j;
 	
 	for(i=jugin;i<=jugfin; i++ ){
 	
	 	for(j=3;j<=4;j++){
	 		
 			tablero [i][j]='X';
		 }
	 }
 		
 }
 
 void curop(char tablero[V][H],int opin,int opfin){
 	int i,j;
 	
 	for(i=opin; i<= opfin; i++){
 		
 		for(j=H-4; j<= H-3;j++){
 		
		 	tablero [i][j]='X';
		 }
 		
	 }
 }
 
 void curbol(char tablero[V][H],int bolx, int boly){
 
   tablero [boly][bolx]='O';
 	
 	
 	
 	
 	
 }
 
 void verejemplo(char tablero[V][H]){
 int i,j;
  
  for(i=0;i<V;i++){
  
  	for(j=0; j<H; j++){
  		printf ("%c",tablero[i][j]);
	  }
  printf("\n");
  
  } 
 
 }
 // Funciones de movimiento oponente y pelota
 
 void nuevojuego(char tablero[V][H],int bolx,int boly,int jugin,int jugfin,int opin,int opfin,int movx,int movy,int movop){
 int gol=0;
 
 do{
 	dibujar(tablero);
 	modificar(tablero,&bolx,&boly,&jugin,&jugfin,&opin,&opfin,&movx,&movy,&movop,&gol);
 	actualizar(tablero,bolx,boly,jugin,jugfin,opin,opfin);
 	Sleep(100);//permite que el programa tarde un poco mas en reiniciar el bucle
 }while(gol==0);	
 	
 }
 
 void dibujar(char tablero[V][H]){
 	system ("cls");
 	verejemplo(tablero);
 	
 }
 
 void modificar(char tablero[V][H],int *bolx,int *boly,int *jugin,int *jugfin,int *opin,int *opfin,int *movx,int *movy,int *movop,int *gol){
int i;
char tecla;
 
 if ((*boly)==1||(*boly)==V-2){
 	 *movy*=-1;
 }
 if ((*bolx)==1|| (*bolx) ==H-2){
 	*gol=1;
 	
 }
 if ((*bolx)==5){
 	
	 for (i=(*jugin);i<=(*jugfin);i++){
 	
	 	if (i==(*boly)){
		*movx*=-1;
		 }
	 }
 }
 
  if ((*bolx)==H-6){
 
  for(i=(*opin);i<=(*opfin);i++ ){
  
  	if(i== (*boly)){
  		*movx*=-1;
	  }
  }
  }	
  if((*opin)==1||(*opfin)==V-1){
  	*movop*=-1;
  }
  
  
  //pelota movimiento
  if((*gol)==0){
  
   (*bolx)+=(*movx);
   (*boly)+=(*movy);
  
  // raqueta op
  
  (*opin)+=(*movop);
  (*opfin)+=(*movop);

//movimiento del jugador
    
	if(kbhit()==1){
    tecla=getch();
	
	if(tecla=='8'){
	  if(*jugin!=1){
	  *jugin-=1;
	  *jugfin-=1;
	  }	
	}	
    	
	}
	if(tecla=='2'){
		if(*jugfin!=V-2){
			*jugin+=1;
			*jugfin+=1;
		}
	}   


}
 }
 
 void actualizar(char tablero[V][H],int bolx,int boly,int jugin,int jugfin,int opin,int opfin){
 	 limite(tablero);
 	
	  curjug(tablero,jugin,jugfin);
 	
 	   curop(tablero,opin,opfin);
 	
 	    curbol(tablero,bolx,boly);
 	
 } 
